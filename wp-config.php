<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * Localized language
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'loki' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/** Régler le bug demande WP ftp*/
define('FS_METHOD', 'direct');

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          '~T)ckw=l:B`V)WE7O}]D6W4 B01##lE15Dnf)z#<=r]-29g#gmag>2Mr{0h)N`nN' );
define( 'SECURE_AUTH_KEY',   'st8*M<Nrm/~PAHapu<%LVp*2:U:0K,ztLocF,`UO-g^u3vZb?M8kIi(cV#<W>Z.*' );
define( 'LOGGED_IN_KEY',     'F@l}9B)Ng<Rw.j!)>@Z-Pfjs||2=$l(Ztk7VUnA&+Y)xu5y5O<>HTSge?EQmp)Eh' );
define( 'NONCE_KEY',         'uFG9pW **5:](_-*36~-a 1-Jbyb t]1lWop{sV}D8XG48SN[v]{2ZKFRGib#97p' );
define( 'AUTH_SALT',         '6!g1@tPmKYp#@$? VdCGD#r5u$U4A[LL!GvXaC8M`<cP6orAN?- ]; $jY<O,k*-' );
define( 'SECURE_AUTH_SALT',  'JSi>X~bcAiWC9Ok]GSpU%E60JbC.x)dVNiDnGKKUd0C>xd]kYk8cOPfm[04#X#VG' );
define( 'LOGGED_IN_SALT',    'ms/o,5}wX0tq7qQd]xRbDsl%7h/<=z*e5J[TVPRklUZnS^ls%Q`bNX}s(r-TImsK' );
define( 'NONCE_SALT',        'p-HuWjO,qZm6VMVF8E@f4>hb8,g)OSV(7K7hL7Aa,Y{ X_deBl/.k&&JlZA+vI5g' );
define( 'WP_CACHE_KEY_SALT', 'u?.}@L(#H^LrVx=)dg/~f_=/3y^*4>@{~|jnvC(pP7Uj-=}a<=6JQwcTfw2tP:-e' );


/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'avg';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );


/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
