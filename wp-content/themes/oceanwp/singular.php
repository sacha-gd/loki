<?php
/**
 * The template for displaying all pages, single posts and attachments
 *
 * This is a new template file that WordPress introduced in
 * version 4.3.
 *
 * @package OceanWP WordPress theme
 */

get_header(); 

$nombre_de_heros = get_field('nombre_de_heros');
$taille = get_field('taille');
$salaire = get_field('salaire');
$sex = get_field('sex');
$equipe = get_field('equipe');

?>

	<?php do_action( 'ocean_before_content_wrap' ); ?>

	<div id="content-wrap" class="container clr">

		<?php do_action( 'ocean_before_primary' ); ?>

		<div id="primary" class="content-area clr">

			<?php do_action( 'ocean_before_content' ); ?>

			<div id="content" class="site-content clr">

				<?php do_action( 'ocean_before_content_inner' ); ?>

				<?php
				// Elementor `single` location.
				if ( ! function_exists( 'elementor_theme_do_location' ) || ! elementor_theme_do_location( 'single' ) ) {

					// Start loop.
					while ( have_posts() ) :
						the_post();

						if ( is_singular( 'download' ) ) {

							// EDD Page.
							get_template_part( 'partials/edd/single' );

						} elseif ( is_singular( 'page' ) ) {

							// Single post.
							get_template_part( 'partials/page/layout' );

						} elseif ( is_singular( 'oceanwp_library' ) || is_singular( 'elementor_library' ) ) {

							// Library post types.
							get_template_part( 'partials/library/layout' );

						} else {

							// All other post types.
							get_template_part( 'partials/single/layout', get_post_type() );

						}

					endwhile;

				}
				?>

				<?php do_action( 'ocean_after_content_inner' ); ?>

			<?php
				if( $nombre_de_heros ): ?>
				<h5>Nombre de héros : <Strong style="color:#88A7D8"><?php the_field('nombre_de_heros');?></Strong></h5>
			<?php endif; ?>

			<?php
				if( $taille ): ?>
				<h5>Taille : <Strong style="color:#88A7D8"><?php the_field('taille');?> m</Strong></h5>
			<?php endif; ?>

			<?php
				if( $salaire ): ?>
				<h5>Salaire : <Strong style="color:#88A7D8"><?php the_field('salaire');?> €</Strong></h5>
			<?php endif; ?>

			<?php
				if( $sex ): ?>
				<h5>Sex : <Strong style="color:#88A7D8"><?php the_field('sex');?></Strong></h5>
			<?php endif; ?>

			<?php
				if( $equipe ): ?>
				<h5>Equipe : <Strong style="color:#88A7D8"><?php the_field('equipe');?></Strong></h5>
			<?php endif; ?>

			
			</div><!-- #content -->

			<?php do_action( 'ocean_after_content' ); ?>

		</div><!-- #primary -->

		<?php do_action( 'ocean_after_primary' ); ?>

	</div><!-- #content-wrap -->

	<?php do_action( 'ocean_after_content_wrap' ); ?>

<?php get_footer(); ?>
