<?php

function add_licence() {
    // Add new "Locations" taxonomy to Posts
    register_taxonomy('location', 'post', array(
      // Hierarchical taxonomy (like categories)
      'hierarchical' => true,
      // This array of options controls the labels displayed in the WordPress Admin UI
      'labels' => array(
        'name' => _x( 'Licences', 'taxonomy general name' ),
        'singular_name' => _x( 'Licence', 'taxonomy singular name' ),
        'search_items' =>  __( 'Rechercher une Licences' ),
        'all_items' => __( 'Tout les Licences' ),
        'parent_item' => __( 'Parent Licence' ),
        'parent_item_colon' => __( 'Parent Licence:' ),
        'edit_item' => __( 'Edit Licence' ),
        'update_item' => __( 'Mettre à jour Licence' ),
        'add_new_item' => __( 'Ajouter une Licence' ),
        'new_item_name' => __( 'Nom du nouveau Licence' ),
        'menu_name' => __( 'Licences' ),
      ),
      // Control the slugs used for this taxonomy
      'rewrite' => array(
        'slug' => 'locations', // This controls the base slug that will display before each term
        'with_front' => false, // Don't display the category base before "/locations/"
        'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
      ),
    ));
  }

  function add_createur() {
    // Add new "createurs" taxonomy to Posts
    register_taxonomy('createur', 'post', array(
      // Hierarchical taxonomy (like categories)
      'hierarchical' => true,
      // This array of options controls the labels displayed in the WordPress Admin UI
      'labels' => array(
        'name' => _x( 'Créateurs', 'taxonomy general name' ),
        'singular_name' => _x( 'Créateur', 'taxonomy singular name' ),
        'search_items' =>  __( 'Rechercher une Créateurs' ),
        'all_items' => __( 'Tout les Créateurs' ),
        'parent_item' => __( 'Parent Créateur' ),
        'parent_item_colon' => __( 'Parent Créateur:' ),
        'edit_item' => __( 'Edit Créateur' ),
        'update_item' => __( 'Mettre à jour Créateur' ),
        'add_new_item' => __( 'Ajouter une Créateur' ),
        'new_item_name' => __( 'Nom du nouveau Créateur' ),
        'menu_name' => __( 'Créateurs' ),
      ),
      // Control the slugs used for this taxonomy
      'rewrite' => array(
        'slug' => 'createurs', // This controls the base slug that will display before each term
        'with_front' => false, // Don't display the category base before "/createurs/"
        'hierarchical' => true // This will allow URL's like "/createurs/boston/cambridge/"
      ),
    ));
  }

  add_action( 'init', 'add_licence', 0 );
  add_action( 'init', 'add_createur', 0 );

?>

