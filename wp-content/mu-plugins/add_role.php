<?php

$result = add_role( 'contrib', __(
    'Contributor' ),
    array(
    'read' => true, // active cette capacité de lecture
    'edit_posts' => true , // Permet à l' utilisateur de modifier ses propres articles
    'edit_pages' => true , // Permet à l' utilisateur d'éditer des pages
    'edit_others_posts' => true , // Permet à l' utilisateur d'éditer d'autres articles pas seulement les siens
    'create_posts' => true , // Permet à l' utilisateur de créer de nouveaux articles
    'manage_categories' => true , // Permet à l' utilisateur de gérer les catégories des articles
    'publish_posts' => true , // Permet à l'utilisateur de publier, sinon les messages reste en mode brouillon
    'edit_themes' => true , // L' utilisateur ne peut pas modifier un thème
    'install_plugins' => true , // L' utilisateur ne peut pas ajouter de nouveaux plugins
    'update_plugin' => true , // L'utilisateur ne peut pas mettre à jour les plugins
    'update_core' => true // l’utilisateur ne peut pas effectuer des mises à jour de WordPress
    )
    );

?>