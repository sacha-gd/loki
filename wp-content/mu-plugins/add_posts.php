<?php

/*
 * Create post type
 */
add_action('init', function() {

    register_post_type('brand', [
        'labels'                => [
            'name'                  => 'Brands',
            'singular_name'         => 'Brand',
        ],
        'public'                => true,
        'has_archive'           => false,
        'rewrite'               => [ 'slug' => 'brand' ],
        'menu_icon'             => 'dashicons-networking',
    ]);

    register_post_type('geo-areas', [
        'labels'                => [
            'name'                  => 'Geographical areas',
            'singular_name'         => 'Geographical area',
        ],
        'public'                => true,
        'has_archive'           => false,
        'rewrite'               => [ 'slug' => 'geo-areas' ],
        'menu_icon'             => 'dashicons-admin-site',
    ]);

});